# Geometry Dash

At first, I would like to say, that this game was been built from [GamesWithGabe's Geometry Dash series](https://youtu.be/K0WrOYaX-TI).

So, almost all credit is from Gabe, not mine :).

---

### Summary:

- [What requirements do I need to run the game?](#-what-requirements-do-i-need-to-run-the-game)

- [How do I run the game?](#-how-do-i-run-the-game)

- [How do I play the Game?](#-how-do-i-play-the-game)

- [Which techniques and rendering stuff were implemented in this game?](#-which-techniques-and-rendering-stuff-were-implemented-in-this-game)

---

#### » What requirements do I need to run the game?

To run this project, you just need to have [jdk 8](https://www.oracle.com/eg/java/technologies/javase/javase-jdk8-downloads.html). I think that [OpenJdk8](https://www.openlogic.com/openjdk-downloads) also is supported, but I don't have sure.

---

### » How do I run the game?

The game is already compiled in an output directory, so, you just need to:

- Copy the assets and levels folder to out/production/Geometry\ Dash/

```bash
cp ~/projectPath/assets ~/projectPath/out/production/Geometry\ Dash/
cp ~/projectPath/levels ~/projectPath/out/production/Geometry\ Dash/
```

- After, we will just go into the package's folder and run the project, with:

```bash
cd ~/projectPath/out/production/Geometry\ Dash/
java com.davidalmarinho.main.Main
```

If you don't know much about bash, I will give you an example.

Let's suppose that you have downloaded the project and, now, the project is in Download's folder.

So, the commands wil be:

```bash
cp ~/Downloads/geometry-dash/assets ~/Downloads/Geometry\ Dash/out/production/Geometry\ Dash/
cp ~/Downloads/geometry-dash/levels ~/Downloads/Geometry\ Dash/out/production/Geometry\ Dash/
cd ~/Downloads/geometry-dash/out/production/Geometry\ Dash/
java com.davidalmarinho.main.Main
```

---

### » How do I play the Game?

It stills in development, so the maximum that I can tell you now is that F1 saves the level, F2 imports the level, F3 saves the level and you can jump using the Space key.

---

### » Which techniques and rendering stuff were implemented in this game?

This project uses some structures and patterns: AssetPool, Transform, Singleton, Serialization and Deserialization, and Entity Component System.

The rendering process is all made by Graphics2D Java's library.

---

How hope you enjoy it :D

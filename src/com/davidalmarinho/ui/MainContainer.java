package com.davidalmarinho.ui;

import com.davidalmarinho.game_objects.GameObject;
import com.davidalmarinho.game_objects.components.BoxBounds;
import com.davidalmarinho.game_objects.components.Component;
import com.davidalmarinho.game_objects.components.Sprite;
import com.davidalmarinho.game_objects.components.Spritesheet;
import com.davidalmarinho.main.data_structures.AssetPool;
import com.davidalmarinho.main.data_structures.Transform;
import com.davidalmarinho.utils.Constants;
import com.davidalmarinho.utils.Vector2;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

public class MainContainer extends Component {
    private List<GameObject> menuItems;

    public MainContainer() {
        this.menuItems = new ArrayList<>();
        init();
    }

    public void init() {
        initAssetPool();
        Spritesheet groundSprite = AssetPool.getSpritesheet("assets/groundSprites.png");
        Spritesheet buttonsSprite = AssetPool.getSpritesheet("assets/ui/buttonSprites.png");

        for (int i = 0; i < groundSprite.sprites.size(); i++) {
            Sprite currentSprite = groundSprite.sprites.get(i);
            int x = Constants.BUTTON_OFFSET_X + (currentSprite.column * Constants.BUTTON_WIDTH) +
                    (currentSprite.column * Constants.BUTTON_SPACING_HZ);
            int y = Constants.BUTTON_OFFSET_Y + (currentSprite.row * Constants.BUTTON_HEIGHT) +
                    (currentSprite.row * Constants.BUTTON_SPACING_VT);

            GameObject obj = new GameObject("Generated", new Transform(new Vector2(x, y)), -1);
            obj.addComponent(currentSprite.copy());
            MenuItem menuItem = new MenuItem(x, y, Constants.BUTTON_WIDTH, Constants.BUTTON_HEIGHT,
                    buttonsSprite.sprites.get(0),buttonsSprite.sprites.get(1));
            obj.addComponent(menuItem);
            obj.addComponent(new BoxBounds(Constants.TILE_WIDTH, Constants.TILE_HEIGHT));
            menuItems.add(obj);
        }
    }

    /*private int containerCoordinate(int ) {
        int coordinate =
    }*/

    private void initAssetPool() {
        AssetPool.addSpritesheet("assets/groundSprites.png", 42, 42,
                2, 6, 12);
        AssetPool.addSpritesheet("assets/ui/buttonSprites.png", 60, 60,
                2, 6, 2);
    }

    @Override
    public void start() {
        for (GameObject g : menuItems) {
            for (Component c : g.getAllComponents()) {
                c.start();
            }
        }
    }

    @Override
    public void update(double dt) {
        for (GameObject g : menuItems) {
            g.update(dt);
        }
    }

    @Override
    public Component copy() {
        return null;
    }

    @Override
    public void draw(Graphics2D g2) {
        for (GameObject g : menuItems) {
            g.draw(g2);
        }
    }

    @Override
    public String serialize(int tabSize) {
        return "";
    }
}

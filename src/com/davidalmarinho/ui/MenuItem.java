package com.davidalmarinho.ui;

import com.davidalmarinho.game_objects.GameObject;
import com.davidalmarinho.game_objects.components.Component;
import com.davidalmarinho.game_objects.components.SnapToGrid;
import com.davidalmarinho.game_objects.components.Sprite;
import com.davidalmarinho.graphics.Window;
import com.davidalmarinho.main.scenes.LevelEditorScene;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

public class MenuItem extends Component {
    private int x, y, width, height;
    private Sprite buttonSprite, hoverSprite, myImage;
    public boolean isSelected;
    private int bufferX, bufferY;

    public MenuItem(int x, int y, int width, int height, Sprite buttonSprite, Sprite hoverSprite) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.buttonSprite = buttonSprite;
        this.hoverSprite = hoverSprite;
    }

    @Override
    public void start() {
        myImage = gameObject.getComponent(Sprite.class);
        this.bufferX = (int) (this.width / 2.0 - myImage.width / 2.0);
        this.bufferY = (int) (this.height / 2.0 - myImage.height / 2.0);
    }

    @Override
    public Component copy() {
        return null;
    }

    @Override
    public void update(double dt) {
        if (!isSelected
                && Window.getInstance().mouseListener.x > this.x && Window.getInstance().mouseListener.x <= this.x + this.width
                && Window.getInstance().mouseListener.y > this.y && Window.getInstance().mouseListener.y <= this.y + this.height) {
            if (Window.getInstance().mouseListener.mousePressed
                    && Window.getInstance().mouseListener.mouseButton == MouseEvent.BUTTON1) {
                GameObject gameObject = this.gameObject.copy();
                gameObject.removeComponent(MenuItem.class);
                LevelEditorScene levelEditorScene = ((LevelEditorScene) Window.getInstance().getCurrentScene());
                SnapToGrid snapToGrid = levelEditorScene.mouseCursor.getComponent(SnapToGrid.class);
                gameObject.addComponent(snapToGrid);
                levelEditorScene.mouseCursor = gameObject;

                isSelected = true;
            }
        }
    }

    @Override
    public void draw(Graphics2D g2) {
        g2.drawImage(this.buttonSprite.image, this.x, this.y, this.width, this.height, null);
        g2.drawImage(myImage.image, this.x + bufferX, this.y + bufferY, myImage.width, myImage.height, null);
        if (isSelected) {
            g2.drawImage(hoverSprite.image, this.x, this.y, this.width, this.height, null);
        }
    }

    @Override
    public String serialize(int tabSize) {
        return "";
    }
}

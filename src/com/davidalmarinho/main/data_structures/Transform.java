package com.davidalmarinho.main.data_structures;

import com.davidalmarinho.file.Parser;
import com.davidalmarinho.file.Serialize;
import com.davidalmarinho.utils.Vector2;

public class Transform extends Serialize {
    public Vector2 position;
    public Vector2 scale;
    // We rotate just a fake coordinate, z, so we can put float here
    public float rotate;

    public Transform(Vector2 position) {
        this.position = position;
        this.scale = new Vector2(1.0f, 1.0f);
        this.rotate = 0.0f;
    }

    public Transform copy() {
        Transform transform = new Transform(this.position.copy());
        transform.scale = this.scale.copy();
        transform.rotate = this.rotate;
        return transform;
    }

    @Override
    public String toString() {
        return "Position(" + position.x + ", " + position.y + ")";
    }

    @Override
    public String serialize(int tabSize) {
        return beginObjectProperty("Transform", tabSize) +
                beginObjectProperty("Position", tabSize + 1) +
                position.serialize(tabSize + 2) +
                closeObjectProperty(tabSize + 1) +
                addEnding(true, true) +

                beginObjectProperty("Scale", tabSize + 1) +
                scale.serialize(tabSize + 2) +
                closeObjectProperty(tabSize + 1) +
                addEnding(true, true) +

                addFloatProperty("rotation", rotate, tabSize + 1, true, false) +
                closeObjectProperty(tabSize);
    }

    public static Transform deserialize() {
        Parser.consumeBeginObjectProperty("Transform");
        Parser.consumeBeginObjectProperty("Position");
        Vector2 position = Vector2.deserialize();
        Parser.consumeEndObjectProperty();

        Parser.consume(',');
        Parser.consumeBeginObjectProperty("Scale");
        Vector2 scale = Vector2.deserialize();
        Parser.consumeEndObjectProperty();

        Parser.consume(',');
        float rotation = Parser.consumeFloatProperty("rotation");
        Parser.consumeEndObjectProperty();

        Transform transform = new Transform(position);
        transform.scale = scale;
        transform.rotate = rotation;

        return transform;
    }
}

package com.davidalmarinho.main.data_structures;

import com.davidalmarinho.game_objects.components.Sprite;
import com.davidalmarinho.game_objects.components.Spritesheet;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class AssetPool {
    /* HashMap contains a key and an other variable (key, variable)
     * Ex.: Map <String, String> teams = new HashMap<>();
     *      Our hash map values are ("", "")
     *      teams.put("Portugal", "Porto");
     *      Our hash map values are ("Portugal", "Porto")
     *
     *      To catch the country's team:
     *      teams.containsKey("Portugal");
     *      To catch the team:
     *      teams.get("Porto");
     *
     * So, with HashMap we can get already existing values.
     * This will turn our code more safe :D
     */
    static Map<String, Sprite> sprites = new HashMap<>();
    static Map<String, Spritesheet> spritesheets = new HashMap<>();

    // Check if exists a sprite
    public static boolean hasSprite(String pictureFile) {
        File tmp = new File(pictureFile);
        return sprites.containsKey(tmp.getAbsolutePath());
    }

    public static boolean hasSpritesheet(String pictureFile) {
        File tmp = new File(pictureFile);
        return spritesheets.containsKey(tmp.getAbsolutePath());
    }

    public static Sprite getSprite(String pictureFile) {
        File file = new File(pictureFile);
        if (!hasSprite(file.getAbsolutePath())) {
            Sprite sprite = new Sprite(pictureFile);
            addSprite(pictureFile, sprite);
        }
        return sprites.get(file.getAbsolutePath());
    }

    public static Spritesheet getSpritesheet(String pictureFile) {
        File file = new File(pictureFile);
        if (hasSpritesheet(file.getAbsolutePath())) {
            return spritesheets.get(file.getAbsolutePath());
        } else {
            System.out.println("Error: Spritesheet '" + pictureFile + "' doesn't exist");
            System.exit(-2);
        }

        return null;
    }

    /**
     *
     * @param pictureFile The absolute path to the picture
     * @param sprite
     */
    public static void addSprite(String pictureFile, Sprite sprite) {
        File file = new File(pictureFile);
        if (!hasSprite(file.getAbsolutePath())) {
            sprites.put(file.getAbsolutePath(), sprite);
        } else {
            System.out.println("Error: AssetPool already has the asset: '" + file.getAbsolutePath() + "'");
            System.exit(-1);
        }
    }

    public static void addSpritesheet(String pictureFile, int tileWidth, int tileHeight, int spacing, int columns, int size) {
        File file = new File(pictureFile);
        if (!hasSpritesheet(file.getAbsolutePath())) {
            Spritesheet spritesheet = new Spritesheet(pictureFile, tileWidth, tileHeight, spacing,
                    columns, size);
            spritesheets.put(file.getAbsolutePath(), spritesheet);
        }
    }
}

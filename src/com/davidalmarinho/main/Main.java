package com.davidalmarinho.main;

import com.davidalmarinho.graphics.Window;

public class Main {
    public static void main(String[] args) {
        Window window = Window.getInstance();
        window.init();

        Thread mainThread = new Thread(window);
        mainThread.start();
    }
}

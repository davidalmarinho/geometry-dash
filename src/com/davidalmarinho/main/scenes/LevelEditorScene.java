package com.davidalmarinho.main.scenes;

import com.davidalmarinho.file.Parser;
import com.davidalmarinho.game_objects.GameObject;
import com.davidalmarinho.game_objects.components.Player;
import com.davidalmarinho.game_objects.components.*;
import com.davidalmarinho.graphics.Window;
import com.davidalmarinho.main.data_structures.AssetPool;
import com.davidalmarinho.main.data_structures.Transform;
import com.davidalmarinho.ui.MainContainer;
import com.davidalmarinho.utils.Constants;
import com.davidalmarinho.utils.Vector2;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class LevelEditorScene extends Scene {
    public GameObject player;
    private GameObject ground;
    private Grid grid;
    private CameraControls cameraControls;
    public GameObject mouseCursor;
    private MainContainer editingButtons;

    public LevelEditorScene(String name) {
        super(name);
        System.out.println("Inside LevelEditor Scene");
    }

    @Override
    public void init() {
        initAssetPool();
        editingButtons = new MainContainer();
        grid = new Grid();
        cameraControls = new CameraControls();
        editingButtons.start();

        mouseCursor = new GameObject("Mouse Cursor", new Transform(new Vector2()), 10);
        mouseCursor.addComponent(new SnapToGrid(Constants.TILE_WIDTH, Constants.TILE_HEIGHT));

        this.player = new GameObject("Some Game Object", new Transform(new Vector2(504.0f, 336.0f)), 0);
        Spritesheet layerOne = AssetPool.getSpritesheet("assets/player/layerOne.png");
        Spritesheet layerTwo = AssetPool.getSpritesheet("assets/player/layerTwo.png");
        Spritesheet layerThree = AssetPool.getSpritesheet("assets/player/layerThree.png");
        Player playerComp = new Player(layerOne.sprites.get(0), layerTwo.sprites.get(0), layerThree.sprites.get(0),
                Color.BLUE, Color.RED);
        player.addComponent(playerComp);

        ground = new GameObject("Ground", new Transform(new Vector2(0, Constants.GROUND_Y)), 1);
        ground.addComponent(new Ground());

        ground.setNonSerializable();
        player.setNonSerializable();
        // Add to rendering list and to the game objects list
        addGameObject(player);
        addGameObject(ground);

        Parser.openFile("Test");
    }

    @Override
    public void update(double dt) {
        // Lock the ground
        if (camera.position.y > Constants.CAMERA_OFFSET_GROUND_Y) {
            camera.position.y = Constants.CAMERA_OFFSET_GROUND_Y;
        }

        for (GameObject gameObject : gameObjects) {
            gameObject.update(dt);
        }

        cameraControls.update(dt);
        grid.update(dt);
        editingButtons.update(dt);
        mouseCursor.update(dt);

        // Export Level
        if (Window.getInstance().keyListener.isKeyPressed(KeyEvent.VK_F1)) {
            export("Test");
        } else if (Window.getInstance().keyListener.isKeyPressed(KeyEvent.VK_F2)) {
            importLevel("Test");
        } else if (Window.getInstance().keyListener.isKeyPressed(KeyEvent.VK_F3)) {
            Window.getInstance().changeScene(1);
        }
    }

    private void export(String fileName) {
        try {
            FileOutputStream fos = new FileOutputStream("levels/" + fileName + ".zip");
            ZipOutputStream zos = new ZipOutputStream(fos);

            zos.putNextEntry(new ZipEntry(fileName + ".json"));

            int i = 0;
            for (GameObject gameObject : gameObjects) {
                String str = gameObject.serialize(0);
                if (str.compareTo("") != 0) {
                    zos.write(str.getBytes());
                    if (i != gameObjects.size() - 1) {
                        zos.write(",\n".getBytes());
                    }
                }
                i++;
            }

            zos.closeEntry();
            zos.close();
            fos.close();

        } catch (IOException ioE) {
            ioE.printStackTrace();
            System.exit(-2);
        }
    }

    public void initAssetPool() {
        AssetPool.addSpritesheet("assets/player/layerOne.png", 42, 42,
                2, 13, 13 * 5);
        AssetPool.addSpritesheet("assets/player/layerTwo.png", 42, 42,
                2, 13, 13 * 5);
        AssetPool.addSpritesheet("assets/player/layerThree.png", 42, 42,
                2, 13, 13 * 5);
    }

    @Override
    public void draw(Graphics2D g2) {
        g2.setColor(new Color(1.0f, 1.0f, 1.0f));
        g2.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

        renderer.render(g2);
        grid.draw(g2);
        editingButtons.draw(g2);

        // Should be drawn last
        mouseCursor.draw(g2);
    }
}

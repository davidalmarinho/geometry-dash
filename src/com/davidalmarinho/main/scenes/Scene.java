package com.davidalmarinho.main.scenes;

import com.davidalmarinho.file.Parser;
import com.davidalmarinho.game_objects.GameObject;
import com.davidalmarinho.game_objects.components.Component;
import com.davidalmarinho.main.Camera;
import com.davidalmarinho.main.Renderer;
import com.davidalmarinho.utils.Vector2;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

public abstract class Scene {
    String name;
    public Camera camera;
    protected List<GameObject> gameObjects;
    protected Renderer renderer;

    public Scene(String name) {
        this.name = name;
        this.camera = new Camera(new Vector2());
        this.gameObjects = new ArrayList<>();
        this.renderer = new Renderer(this.camera);
    }

    public abstract void init();

    public void addGameObject(GameObject gameObject) {
        gameObjects.add(gameObject);
        renderer.submit(gameObject);
        for (Component c : gameObject.getAllComponents()) {
            c.start();
        }
    }

    protected void importLevel(String fileName) {
        Parser.openFile(fileName);

        GameObject go = Parser.parseGameObject();
        while (go != null) {
            addGameObject(go);
            go = Parser.parseGameObject();
        }
    }

    public abstract void update(double dt);
    public abstract void draw(Graphics2D g2);
}

package com.davidalmarinho.main.scenes;

import com.davidalmarinho.game_objects.GameObject;
import com.davidalmarinho.game_objects.components.*;
import com.davidalmarinho.main.data_structures.AssetPool;
import com.davidalmarinho.main.data_structures.Transform;
import com.davidalmarinho.utils.Constants;
import com.davidalmarinho.utils.Vector2;

import java.awt.Color;
import java.awt.Graphics2D;

public class LevelScene extends Scene {
    public GameObject player;
    public BoxBounds playerBounds;
    static LevelScene levelScene;

    public LevelScene(String name) {
        super(name);
        System.out.println("Inside Level Scene");
    }

    @Override
    public void init() {
        initAssetPool();
        this.player = new GameObject("Some Game Object", new Transform(new Vector2(500.0f, 300.0f)), 0);
        Spritesheet layerOne = AssetPool.getSpritesheet("assets/player/layerOne.png");
        Spritesheet layerTwo = AssetPool.getSpritesheet("assets/player/layerTwo.png");
        Spritesheet layerThree = AssetPool.getSpritesheet("assets/player/layerThree.png");
        Player playerComp = new Player(layerOne.sprites.get(0), layerTwo.sprites.get(0), layerThree.sprites.get(0),
                Color.BLUE, Color.RED);
        player.addComponent(playerComp);
        player.addComponent(new RigidBody(new Vector2(Constants.PLAYER_SPEED, 0)));
        player.addComponent(new BoxBounds(Constants.PLAYER_WIDTH, Constants.PLAYER_HEIGHT));
        playerBounds = new BoxBounds(Constants.TILE_WIDTH, Constants.TILE_HEIGHT);
        player.addComponent(playerBounds);

        renderer.submit(player);

        initBackgrounds();

        importLevel("Test");
    }

    private void initBackgrounds() {
        GameObject ground = new GameObject("Ground", new Transform(new Vector2(0, Constants.GROUND_Y)), 1);
        ground.addComponent(new Ground());

        addGameObject(ground);

        int numberBackgrounds = 7;
        GameObject[] backgrounds = new GameObject[numberBackgrounds];
        GameObject[] groundsBgs = new GameObject[numberBackgrounds];

        for (int i = 0; i < numberBackgrounds; i++) {
            ParallaxBackground bg = new ParallaxBackground("assets/backgrounds/bg01.png", backgrounds,
                    ground.getComponent(Ground.class), false);
            int x = i * bg.sprite.width;
            int y = 0;

            GameObject go = new GameObject("Background", new Transform(new Vector2(x, y)), -10);
            go.setUi(true);
            go.addComponent(bg);
            backgrounds[i] = go;

            ParallaxBackground groundBg = new ParallaxBackground("assets/grounds/ground01.png",
                    groundsBgs, ground.getComponent(Ground.class), true);

            x = i * groundBg.sprite.width;
            y = bg.sprite.height;

            GameObject groundGo = new GameObject("GroundBg", new Transform(new Vector2(x, y)), -9);
            groundGo.addComponent(groundBg);
            groundGo.setUi(true);
            groundsBgs[i] = groundGo;

            addGameObject(go);
            addGameObject(groundGo);
        }
    }

    @Override
    public void update(double dt) {
        // If player moving right, move camera with him
        if (player.transform.position.x - camera.position.x > Constants.CAMERA_OFFSET_X) {
            camera.position.x = player.transform.position.x - Constants.CAMERA_OFFSET_X;
        }

        // If player jumping or falling, camera moves with him
        if (player.transform.position.y - camera.position.y > Constants.CAMERA_OFFSET_Y) {
            camera.position.y = player.transform.position.y - Constants.CAMERA_OFFSET_Y;
        }

        // Lock the ground
        if (camera.position.y > Constants.CAMERA_OFFSET_GROUND_Y) {
            camera.position.y = Constants.CAMERA_OFFSET_GROUND_Y;
        }

        player.update(dt);
        player.getComponent(Player.class).onGround = false;
        for (GameObject gameObject : gameObjects) {
            gameObject.update(dt);

            Bounds b = gameObject.getComponent(Bounds.class);
            if (b != null) {
                if (Bounds.checkCollision(playerBounds, b)) {
                    Bounds.resolveCollision(b, player);
                }
            }
        }
    }

    private void initAssetPool() {
        AssetPool.addSpritesheet("assets/player/layerOne.png", 42, 42,
                2, 13, 13 * 5);
        AssetPool.addSpritesheet("assets/player/layerTwo.png", 42, 42,
                2, 13, 13 * 5);
        AssetPool.addSpritesheet("assets/player/layerThree.png", 42, 42,
                2, 13, 13 * 5);
    }

    @Override
    public void draw(Graphics2D g2) {
        g2.setColor(Constants.BG_COLOR);
        g2.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
        renderer.render(g2);
    }

    public static LevelScene get() {
        if (levelScene == null) {
            levelScene = new LevelScene("Level Scene");
        }

        return levelScene;
    }
}

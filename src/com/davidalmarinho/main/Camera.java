package com.davidalmarinho.main;

import com.davidalmarinho.utils.Vector2;

public class Camera {
    public Vector2 position;

    public Camera(Vector2 position) {
        this.position = position;
    }
}

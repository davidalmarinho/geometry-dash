package com.davidalmarinho.main;

import com.davidalmarinho.game_objects.GameObject;
import com.davidalmarinho.main.data_structures.Transform;
import com.davidalmarinho.utils.Vector2;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Renderer {
    Map<Integer, List<GameObject>> gameObjects;
    Camera camera;

    public Renderer(Camera camera) {
        this.camera = camera;
        this.gameObjects = new HashMap<>();
    }

    // We want to submit a object to be draw...
    public void submit(GameObject gameObject) {
        gameObjects.computeIfAbsent(gameObject.zIndex, k -> new ArrayList<>());
        gameObjects.get(gameObject.zIndex).add(gameObject);
    }

    public void render(Graphics2D g2) {
        int lowestZIndex = Integer.MAX_VALUE;
        int highestZIndex = Integer.MIN_VALUE;
        for (Integer i : gameObjects.keySet()) {
            if (i < lowestZIndex) lowestZIndex = i;
            if (i > highestZIndex) highestZIndex = i;
        }

        int currentZIndex = lowestZIndex;
        while (currentZIndex <= highestZIndex) {
            if (gameObjects.get(currentZIndex) == null) {
                currentZIndex++;
                continue;
            }

            for (GameObject gameObject : gameObjects.get(currentZIndex)) {
                if (gameObject.isUi()) {
                    gameObject.draw(g2);
                } else {
                    // Keep the old coordinates and settings of our game object
                    Transform oldTransform = new Transform(gameObject.transform.position);
                    oldTransform.rotate = gameObject.transform.rotate;
                    // oldTransform.scale = new Vector2(gameObject.transform.scale.x, gameObject.transform.scale.y);
                    oldTransform.scale = gameObject.transform.scale;

                    // Camera's logic
                    gameObject.transform.position = new Vector2(gameObject.transform.position.x - camera.position.x,
                            gameObject.transform.position.y - camera.position.y);
                    gameObject.draw(g2);

                    // Replacing the player's position
                    gameObject.transform = oldTransform;
                }
            }

            currentZIndex++;
        }
    }
}

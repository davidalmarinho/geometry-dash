package com.davidalmarinho.file;

import com.davidalmarinho.game_objects.GameObject;
import com.davidalmarinho.game_objects.components.BoxBounds;
import com.davidalmarinho.game_objects.components.Component;
import com.davidalmarinho.game_objects.components.Sprite;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class Parser {
    private static int offset = 0;
    private static int line = 1;
    private static byte[] bytes; // 'a', 'b', 'c', 'd',...

    public static void openFile(String filename) {
        // Check if file exists
        File temp = new File("levels/" + filename + ".zip");
        if (!temp.exists()) return;

        try {
            ZipFile zipFile = new ZipFile("levels/" + filename + ".zip");
            ZipEntry jsonFile = new ZipEntry(filename + ".json");
            InputStream stream = zipFile.getInputStream(jsonFile);

            // Copy stream's data
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int len;
            while ((len = stream.read(buffer)) > -1) {
                baos.write(buffer, 0, len);
            }
            baos.flush();
            Parser.bytes = baos.toByteArray();


        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static GameObject parseGameObject() {
        if (bytes.length == 0 || atEnd()) return null; // We are in the end of the file

        if (peek() == ',') Parser.consume(',');
        skipWhiteSpace();
        if (atEnd()) return null;

        return GameObject.deserialize();
    }

    public static void skipWhiteSpace() {
        while (!atEnd() && (peek() == ' ' || peek() == '\n' || peek() == '\t') || peek() == '\r') {
            // If new line, increase new line
            if (peek() == '\n') {
                line++;
            }
            advance();
        }
    }

    public static char peek() {
        return (char)bytes[offset];
    }

    public static char advance() {
        char c = (char)bytes[offset];
        offset++;
        return c;
    }

    public static void consume(char character) {
        char actual = peek();
        if (actual != character) {
            System.out.println("Error: Expected '" + character + "' but, instead got '"
                    + actual + "' at line '" + line + "'");
            System.exit(-1);
        }
        offset++;
    }

    public static boolean atEnd() {
        return offset == bytes.length;
    }

    public static int parseInt() {
        skipWhiteSpace();
        char alpha;
        StringBuilder stringBuilder = new StringBuilder();
        while (!atEnd() && isDigit(peek()) || peek() == '-') {
            alpha = advance();
            stringBuilder.append(alpha);
        }

        return Integer.parseInt(stringBuilder.toString());
    }

    public static double parseDouble() {
        skipWhiteSpace();
        char alpha;
        StringBuilder stringBuilder = new StringBuilder();
        while (!atEnd() && (isDigit(peek()) || peek() == '.' || peek() == '-')) {
            alpha = advance();
            stringBuilder.append(alpha);
        }

        return Double.parseDouble(stringBuilder.toString());
    }

    public static float parseFloat() {
        float f = (float) parseDouble();
        consume('f');
        return f;
    }

    public static String parseString() {
        skipWhiteSpace();
        char alpha;
        StringBuilder stringBuilder = new StringBuilder();
        consume('"');
        while (!atEnd() && peek() != '"') {
            alpha = advance();
            stringBuilder.append(alpha);
        }
        consume('"');

        return stringBuilder.toString();
    }

    public static boolean parseBoolean() {
        skipWhiteSpace();
        StringBuilder stringBuilder = new StringBuilder();

        if (!atEnd() && peek() == 't') {
            stringBuilder.append("true");
            consume('t');
            consume('r');
            consume('u');
            consume('e');
        } else if (!atEnd() && peek() == 'f') {
            stringBuilder.append("false");
            consume('f');
            consume('a');
            consume('l');
            consume('s');
            consume('e');
        } else {
            System.out.println("Error: Expecting 'true' or 'false', instead got: '" + peek() + "' at line '" + line + "'");
            System.exit(-1);
        }

        return stringBuilder.toString().compareTo("true") == 0;
    }

    private static boolean isDigit(char alpha) {
        return alpha >= '0' && alpha <= '9';
    }

    public static Component parseComponent() {
        String componentTitle = Parser.parseString();
        skipWhiteSpace();
        Parser.consume(':');
        skipWhiteSpace();
        Parser.consume('{');
        switch (componentTitle) {
            case "Sprite":
                return Sprite.deserialize();
            case "BoxBounds":
                return BoxBounds.deserialize();
            default:
                System.out.println("Couldn't find component '" + componentTitle + "' at line: " + Parser.line);
                System.exit(-1);
        }

        return null;
    }

    public static void consumeEndObjectProperty() {
        skipWhiteSpace();
        consume('}');
    }

    public static String consumeStringProperty(String name) {
        skipWhiteSpace();
        checkString(name);
        consume(':');
        return parseString();
    }

    public static int consumeIntProperty(String name) {
        skipWhiteSpace();
        checkString(name);
        consume(':');
        return parseInt();
    }

    public static float consumeFloatProperty(String name) {
        skipWhiteSpace();
        checkString(name);
        consume(':');
        return parseFloat();
    }

    public static double consumeDouble(String name) {
        skipWhiteSpace();
        checkString(name);
        consume(':');
        return parseDouble();
    }

    public static boolean consumeBoolean(String name) {
        skipWhiteSpace();
        checkString(name);
        consume(':');
        return parseBoolean();
    }

    public static void consumeBeginObjectProperty(String name) {
        skipWhiteSpace();
        checkString(name);
        consume(':');
        skipWhiteSpace();
        consume('{');
    }

    private static void checkString(String str) {
        String title = Parser.parseString();
        if (title.compareTo(str) != 0) {
            System.out.println("Expected '" + str + "' instead got '" + title + "' at line: " + Parser.line);
            System.exit(-1);
        }
    }
}

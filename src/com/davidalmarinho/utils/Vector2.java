package com.davidalmarinho.utils;

import com.davidalmarinho.file.Parser;
import com.davidalmarinho.file.Serialize;

public class Vector2 extends Serialize {
    // (x, y)
    public float x, y;

    public Vector2(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Vector2() {
        this.x = 0;
        this.y = 0;
    }

    public Vector2 copy() {
        return new Vector2(this.x, this.y);
    }

    @Override
    public String serialize(int tabSize) {
        return addFloatProperty("x", x, tabSize, true, true) +
                addFloatProperty("y", y, tabSize, true, false);
    }

    public static Vector2 deserialize() {
        float x = Parser.consumeFloatProperty("x");
        Parser.consume(',');
        float y = Parser.consumeFloatProperty("y");

        return new Vector2(x, y);
    }
}

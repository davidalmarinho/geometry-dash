package com.davidalmarinho.graphics;

import com.davidalmarinho.inputs.KeyListener;
import com.davidalmarinho.inputs.MouseListener;
import com.davidalmarinho.main.scenes.LevelEditorScene;
import com.davidalmarinho.main.scenes.LevelScene;
import com.davidalmarinho.main.scenes.Scene;
import com.davidalmarinho.utils.Constants;
import com.davidalmarinho.utils.Time;
import com.davidalmarinho.utils.Vector2;

import javax.swing.JFrame;
import javax.swing.WindowConstants;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

public class Window extends JFrame implements Runnable {
    // Core
    private boolean running = true;
    private Scene currentScene = null;
    public boolean inLevelEditorScene = true;
    // Components
    private static Window window;
    public MouseListener mouseListener;
    public KeyListener keyListener;
    // Background
    private Image doubleBufferImage = null;
    private Graphics doubleBufferGraphics = null;

    private Window() {
        this.mouseListener = new MouseListener();
        this.keyListener = new KeyListener();

        this.setSize(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
        this.setTitle(Constants.SCREEN_TITLE);
        this.setResizable(true);
        this.setMinimumSize(new Dimension(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT));
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.addKeyListener(keyListener);
        this.addMouseListener(mouseListener);
        this.addMouseMotionListener(mouseListener); // Stores movement
        this.setLocationRelativeTo(null);
    }

    public void init() {
        changeScene(0);
    }

    public Scene getCurrentScene() {
        return currentScene;
    }

    public void changeScene(int scene) {
        switch (scene) {
            case 0:
                inLevelEditorScene = true;
                currentScene = new LevelEditorScene("Level Editor");
                currentScene.init();
                break;
            case 1:
                inLevelEditorScene = false;
                currentScene = new LevelScene("Level Scene");
                currentScene.init();
                break;
            default:
                System.out.println("Do not know what this scene this.");
        }
    }

    public void update(double dt) {
        //System.out.println(mouseListener.dx);
        //System.out.println(keyListener.isKeyPressed(KeyEvent.VK_ENTER));
        //System.out.println("FPS: " + 1.0f / dt);
        currentScene.update(dt);
        draw(getGraphics());
    }

    public static Window getInstance() {
        if (window == null) {
            window = new Window();
        }
        return window;
    }

    public void draw(Graphics g) {
        if (doubleBufferImage == null) {
            doubleBufferImage = createImage(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
        }
        doubleBufferGraphics = doubleBufferImage.getGraphics();

        renderOffScreen(doubleBufferGraphics);

        // To, when maximising Window, we can get cool coordinates with mouse :)
        Constants.BACKGROUND_SCALE = new Vector2((float) getWidth() / doubleBufferImage.getWidth(this),
                (float) getHeight() / doubleBufferImage.getHeight(this));

        g.drawImage(doubleBufferImage, 0, 0, getWidth(), getHeight(), null);
    }

    public void renderOffScreen(Graphics g) {
        Graphics2D g2D = (Graphics2D) g;
        currentScene.draw(g2D);
    }

    @Override
    public void run() {
        double lastFrameTime = 0.0;
        try {
            while (running) {
                double time = Time.getTime();
                double delta = time - lastFrameTime;
                lastFrameTime = time;

                update(delta);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

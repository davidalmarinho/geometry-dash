package com.davidalmarinho.game_objects;

import com.davidalmarinho.file.Parser;
import com.davidalmarinho.file.Serialize;
import com.davidalmarinho.game_objects.components.Component;
import com.davidalmarinho.main.data_structures.Transform;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class GameObject extends Serialize {
    private List<Component> components;
    // To debug
    public final String name;
    public Transform transform;
    private boolean serializable = true;
    private boolean isUi;
    public int zIndex;

    public GameObject(String name, Transform transform, int zIndex) {
        this.name = name;
        this.transform = transform;
        this.components = new ArrayList<>();
        this.zIndex = zIndex;
    }

    public <T extends Component> T getComponent(Class<T> componentClass) {
        for (Component c : components) {
            if (componentClass.isAssignableFrom(c.getClass())) {
                try {
                    return componentClass.cast(c);
                } catch (ClassCastException e) {
                    System.out.println("Error: " + componentClass + " class" + " can't cast " + c + " class.");
                    e.printStackTrace();
                    System.exit(-1);
                }
            }
        }
        // No component found
        return null;
    }

    public <T extends Component> void removeComponent(Class<T> componentClass) {
        for (Component c : components) {
            if (c.getClass().isAssignableFrom(c.getClass())) {
                components.remove(componentClass);
                return;
            }
        }
    }

    public void addComponent(Component c) {
        if (c != null) {
            components.add(c);
            c.gameObject = this;
        }
    }

    public List<Component> getAllComponents() {
        return this.components;
    }

    public GameObject copy() {
        GameObject newGameObject = new GameObject("GameObject Generated", transform.copy(), this.zIndex);
        for (Component c : components) {
            Component copy = c.copy();
            if (copy != null) {
                newGameObject.addComponent(copy);
            }
        }
        return newGameObject;
    }

    public void update(double dt) {
        for (Component c : components) {
            c.update(dt);
        }
    }

    public void draw(Graphics2D g2) {
        for (Component c : components) {
            c.draw(g2);
        }
    }

    @Override
    public String serialize(int tabSize) {
        if (!serializable) {
            return "";
        }

        StringBuilder builder = new StringBuilder();

        // Start serializing game object
        builder.append(beginObjectProperty("GameObject", tabSize));

        // Transform
        builder.append(transform.serialize(tabSize + 1));
        builder.append(addEnding(true, true));

        // Name
        builder.append(addStringProperty("Name", name, tabSize + 1, true, true));

        // ZIndex
        if (components.size() > 0) {
            builder.append(addIntProperty("ZIndex", zIndex, tabSize + 1, true, true));
            builder.append(beginObjectProperty("Components", tabSize + 1));
        } else {
            builder.append(addIntProperty("ZIndex", zIndex, tabSize + 1, true, false));
        }

        int i = 0;
        for (Component c : components) {
            String str = c.serialize(tabSize + 2);
            if (str.compareTo("") != 0) {
                builder.append(str);
                if (i != components.size() - 1) {
                    builder.append(addEnding(true, true));
                } else {
                    builder.append(addEnding(true, false));
                }
            }
            i++;
        }

        if (components.size() > 0) {
            builder.append(closeObjectProperty(tabSize + 1));
        }

        builder.append(addEnding(true, false));
        builder.append(closeObjectProperty(tabSize));
        return builder.toString();
    }

    public void setNonSerializable() {
        this.serializable = false;
    }

    public static GameObject deserialize() {
        Parser.consumeBeginObjectProperty("GameObject");
        Transform transform = Transform.deserialize();
        Parser.consume(',');
        String name = Parser.consumeStringProperty("Name");
        Parser.consume(',');
        int zIndex = Parser.consumeIntProperty("ZIndex");

        GameObject gameObject = new GameObject(name, transform, zIndex);

        if (Parser.peek() == ',') {
            // We know that has to be components
            Parser.consume(',');

            Parser.consumeBeginObjectProperty("Components");

            gameObject.addComponent(Parser.parseComponent());

            while (Parser.peek() == ',') {
                Parser.consume(',');
                gameObject.addComponent(Parser.parseComponent());
            }
            Parser.consumeEndObjectProperty();

        }
        Parser.consumeEndObjectProperty();

        return gameObject;
    }

    public boolean isUi() {
        return isUi;
    }

    public void setUi(boolean val) {
        this.isUi = val;
    }
}

package com.davidalmarinho.game_objects.components;

import com.davidalmarinho.graphics.Window;

import java.awt.event.MouseEvent;

public class CameraControls extends Component {
    private float prevMouseX, prevMouseY;

    public CameraControls() {
        prevMouseX = 0.0f;
        prevMouseY = 0.0f;
    }

    @Override
    public void update(double dt) {
        Window window = Window.getInstance();
        if (window.mouseListener.mousePressed && window.mouseListener.mouseButton == MouseEvent.BUTTON3) {
            float dx = (window.mouseListener.x + window.mouseListener.dx - prevMouseX);
            float dy = (window.mouseListener.y + window.mouseListener.dy - prevMouseY);

            window.getCurrentScene().camera.position.x -= dx;
            window.getCurrentScene().camera.position.y -= dy;
        }

        prevMouseX = window.mouseListener.x + window.mouseListener.dx;
        prevMouseY = window.mouseListener.y + window.mouseListener.dy;
    }

    @Override
    public Component copy() {
        return null;
    }

    @Override
    public String serialize(int tabSize) {
        return "";
    }
}

package com.davidalmarinho.game_objects.components;

import com.davidalmarinho.game_objects.GameObject;

public abstract class Bounds extends Component {
    public Bounds() {

    }

    enum BoundsTypes {
        BOX, TRIANGLE
    }

    public BoundsTypes type;
    abstract public float getWidth();
    abstract public float getHeight();

    public static boolean checkCollision(Bounds b1, Bounds b2) {
        // If these type of bounds are

        // Boxs
        if (b1.type == b2.type && b1.type == BoundsTypes.BOX) {
            return BoxBounds.checkCollision((BoxBounds) b1, (BoxBounds) b2);
        }

        return false;
    }

    public static void resolveCollision(Bounds b, GameObject p) {
        if (b.type == BoundsTypes.BOX) {
            BoxBounds box = (BoxBounds) b;
            box.resolveCollision(p);
        }
    }
}

package com.davidalmarinho.game_objects.components;

import com.davidalmarinho.graphics.Window;
import com.davidalmarinho.main.Camera;
import com.davidalmarinho.utils.Constants;

import java.awt.*;
import java.awt.geom.Line2D;

public class Grid extends Component {
    Camera camera;
    public int width, height;
    private final int numYLines = 31;
    private final int numXLines = 16;

    public Grid() {
        this.camera = Window.getInstance().getCurrentScene().camera;
        this.width = Constants.TILE_WIDTH;
        this.height = Constants.TILE_HEIGHT;
    }

    @Override
    public Component copy() {
        return null;
    }

    @Override
    public void update(double dt) {

    }

    @Override
    public void draw(Graphics2D g2) {
        // Make grid lines look beautiful :D
        g2.setStroke(new BasicStroke(1f));
        g2.setColor(new Color(0.2f, 0.2f, 0.2f, 0.5f));

        float startX = (float) (Math.floor(camera.position.x / width) * width - camera.position.x);
        float startY = (float) (Math.floor(camera.position.y / height) * height - camera.position.y);

        float bottom = Math.min(Constants.GROUND_Y - camera.position.y, Constants.SCREEN_HEIGHT);

        // Draw columns
        for (int column = 0; column <= numYLines; column++) {
            g2.draw(new Line2D.Float(startX, 0, startX, bottom));
            startX += width;
        }

        // Draw rows
        for (int rows = 0; rows <= numXLines; rows++) {
            if (camera.position.y + startY < Constants.GROUND_Y) {
                g2.draw(new Line2D.Float(0, startY, Constants.SCREEN_WIDTH, startY));
                startY += height;
            }
        }
    }

    @Override
    public String serialize(int tabSize) {
        return "";
    }
}

package com.davidalmarinho.game_objects.components;

import com.davidalmarinho.file.Parser;
import com.davidalmarinho.main.data_structures.AssetPool;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Sprite extends Component {
    public BufferedImage image;
    private String pictureFile;
    public int width, height;

    public boolean isSubsprite;
    public int row, column, index;

    public Sprite(String pictureFile) {
        this.pictureFile = pictureFile;

        // Load image
        try {
            File file = new File(pictureFile);

            if (AssetPool.hasSprite(pictureFile))
                throw new IOException("Asset already exists: '" + pictureFile + "'");

            // Setting attributes
            this.image = ImageIO.read(file);
            this.width = image.getWidth();
            this.height = image.getHeight();
        } catch (IOException e) {
            System.out.println("Error: Couldn't find '" + pictureFile + "' file.");
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public Sprite(BufferedImage image, String pictureFile) {
        this.image = image;
        this.width = image.getWidth();
        this.height = image.getHeight();
        this.pictureFile = pictureFile;
    }

    public Sprite(BufferedImage image, int row, int column, int index, String pictureFile) {
        this.image = image;
        this.width = image.getWidth();
        this.height = image.getHeight();
        this.row = row;
        this.column = column;
        this.index = index;
        this.isSubsprite = true;
        this.pictureFile = pictureFile;
    }

    @Override
    public Component copy() {
        if (!isSubsprite)
            return new Sprite(this.image, this.pictureFile);
        else
            return new Sprite(this.image, this.row, this.column, this.index, this.pictureFile);
    }

    @Override
    public void draw(Graphics2D g2) {
        g2.drawImage(image, (int) gameObject.transform.position.x, (int) gameObject.transform.position.y,
                width, height, null);
    }

    @Override
    public String serialize(int tabSize) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(beginObjectProperty("Sprite", tabSize));
        stringBuilder.append(addBooleanProperty("isSubsprite", isSubsprite, tabSize + 1, true, true));

        if (isSubsprite) {
            stringBuilder.append(addStringProperty("FilePath", pictureFile, tabSize + 1, true, true));
            stringBuilder.append(addIntProperty("row", row, tabSize + 1, true, true));
            stringBuilder.append(addIntProperty("column", column, tabSize + 1, true, true));
            stringBuilder.append(addIntProperty("index", index, tabSize + 1, true, false));
            stringBuilder.append(closeObjectProperty(tabSize));

            return stringBuilder.toString();
        }

        stringBuilder.append(addStringProperty("FilePath", pictureFile, tabSize + 1, true, false));
        stringBuilder.append(closeObjectProperty(tabSize));
        return stringBuilder.toString();
    }

    public static Sprite deserialize() {
        boolean isSubsprite = Parser.consumeBoolean("isSubsprite");
        Parser.consume(',');
        String filePath = Parser.consumeStringProperty("FilePath");

        if (isSubsprite) {
            Parser.consume(',');
            Parser.consumeIntProperty("row");
            Parser.consume(',');
            Parser.consumeIntProperty("column");
            Parser.consume(',');
            int index = Parser.consumeIntProperty("index");
            if (!AssetPool.hasSpritesheet(filePath)) {
                System.out.println("Spritesheet '" + filePath + "' hasn't been loaded yet.");
                System.exit(-1);
            }
            Parser.consumeEndObjectProperty();
            return (Sprite) AssetPool.getSpritesheet(filePath).sprites.get(index).copy();
        }

        if (!AssetPool.hasSprite(filePath)) {
            System.out.println("Sprite '" + filePath + "' hasn't been loaded yet.");
            System.exit(-1);
        }

        Parser.consumeEndObjectProperty();
        return (Sprite) AssetPool.getSprite(filePath).copy();
    }
}

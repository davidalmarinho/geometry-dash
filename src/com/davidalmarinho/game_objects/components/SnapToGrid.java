package com.davidalmarinho.game_objects.components;

import com.davidalmarinho.game_objects.GameObject;
import com.davidalmarinho.graphics.Window;
import com.davidalmarinho.utils.Constants;
import com.davidalmarinho.utils.Vector2;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

public class SnapToGrid extends Component {
    // Sprite that we will be using to place in the grid
    private Sprite sprite = null;

    // Timers, every 0.2 seconds we do mouse's action
    private float debouncdTime = 0.2f;
    private float debounceLeft = 0.0f;

    private int gridWidth, gridHeight;

    public SnapToGrid(int gridWidth, int gridHeight) {
        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;
    }

    @Override
    public Component copy() {
        return null;
    }

    @Override
    public void update(double dt) {
        debounceLeft -= dt;

        if (gameObject.getComponent(Sprite.class) != null) {
            Window window = Window.getInstance();
            float x = (float) Math.floor((window.getCurrentScene().camera.position.x + window.mouseListener.x +
                    window.mouseListener.dx) / gridWidth);
            float y = (float) Math.floor((window.getCurrentScene().camera.position.y + window.mouseListener.y +
                    window.mouseListener.dy) / gridWidth);
            // Transform into world coordinates current space: x * gridWidth, - Window...camera to place the coordinates on screen
            gameObject.transform.position.x = x * gridWidth - window.getCurrentScene().camera.position.x;
            gameObject.transform.position.y = y * gridHeight - window.getCurrentScene().camera.position.y;

            if (window.mouseListener.y < Constants.BUTTON_OFFSET_Y &&
                    window.mouseListener.mousePressed && window.mouseListener.mouseButton == MouseEvent.BUTTON1
                    && debounceLeft < 0) {
                debounceLeft = debouncdTime;
                GameObject object = gameObject.copy();
                object.transform.position = new Vector2(x * gridWidth, y * gridHeight);
                window.getCurrentScene().addGameObject(object);
            }
        }
    }

    @Override
    public void draw(Graphics2D g2) {
        Sprite sprite = gameObject.getComponent(Sprite.class);
        if (sprite != null) {
            // Edit image to put in it opacity
            float alpha = 0.8f;
            AlphaComposite alphaComposite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
            g2.setComposite(alphaComposite);
            g2.drawImage(sprite.image, (int) gameObject.transform.position.x, (int) gameObject.transform.position.y,
                    sprite.width, sprite.height, null);
            // Reset alphaComposite
            alpha = 1.0f;
            alphaComposite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
            g2.setComposite(alphaComposite);
        }
    }

    @Override
    public String serialize(int tabSize) {
        return "";
    }
}

package com.davidalmarinho.game_objects.components;

import com.davidalmarinho.file.Serialize;
import com.davidalmarinho.game_objects.GameObject;

import java.awt.Graphics2D;

public abstract class Component<T> extends Serialize {
    public GameObject gameObject;

    public abstract Component copy();

    public void start() {
        return;
    }

    public void update(double dt) {
        return;
    }

    public void draw(Graphics2D g2) {
        return;
    }
}

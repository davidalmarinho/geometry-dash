package com.davidalmarinho.game_objects.components;

import com.davidalmarinho.graphics.Window;
import com.davidalmarinho.utils.Constants;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.geom.AffineTransform;

public class Player extends Component {
    Sprite layerOne, layerTwo, layerThree;
    private int width, height;
    public boolean onGround = true;

    public Player(Sprite layerOne, Sprite layerTwo, Sprite layerThree,
                  Color colorOne, Color colorTwo) {
        this.layerOne = layerOne;
        this.layerTwo = layerTwo;
        this.layerThree = layerThree;

        this.width = Constants.PLAYER_WIDTH;
        this.height = Constants.PLAYER_HEIGHT;

        int threshold = 200;

        // Layer one
        for (int y = 0; y < layerOne.image.getHeight(); y++) {
            for (int x = 0; x < layerOne.image.getWidth(); x++) {
                Color color = new Color(layerOne.image.getRGB(x, y));
                // If all colors are greater than 255
                if (color.getRed() > threshold && color.getGreen() > threshold && color.getBlue() > threshold) {
                    layerOne.image.setRGB(x, y, colorOne.getRGB());
                }
            }
        }

        // Layer two
        for (int y = 0; y < layerTwo.image.getHeight(); y++) {
            for (int x = 0; x < layerTwo.image.getWidth(); x++) {
                Color color = new Color(layerTwo.image.getRGB(x, y));
                // If all colors are greater than 255
                if (color.getRed() > threshold && color.getGreen() > threshold && color.getBlue() > threshold) {
                    layerTwo.image.setRGB(x, y, colorTwo.getRGB());
                }
            }
        }

        // Layer three
        for (int y = 0; y < layerThree.image.getHeight(); y++) {
            for (int x = 0; x < layerThree.image.getWidth(); x++) {
                Color color = new Color(layerThree.image.getRGB(x, y));
                // If all colors are greater than 255
                if (color.getRed() > threshold && color.getGreen() > threshold && color.getBlue() > threshold) {
                    layerThree.image.setRGB(x, y, colorOne.getRGB());
                }
            }
        }
    }

    @Override
    public Component copy() {
        return null;
    }

    @Override
    public void update(double dt) {
        if (onGround && Window.getInstance().keyListener.isKeyPressed(KeyEvent.VK_SPACE)) {
            addJumpForce();
            this.onGround = false;
        }

        if (!onGround) {
            gameObject.transform.rotate += 4.0f * dt;
        } else {
            // gameObject.transform.rotate = 0;

            gameObject.transform.rotate = (int) gameObject.transform.rotate % 360;
            if (gameObject.transform.rotate > 180 && gameObject.transform.rotate < 360) {
                gameObject.transform.rotate = 0;
            } else if (gameObject.transform.rotate > 0 && gameObject.transform.rotate < 180) {
                gameObject.transform.rotate = 0;
            }
        }
    }

    private void addJumpForce() {
        gameObject.getComponent(RigidBody.class).velocity.y = Constants.JUMP_FORCE;
    }

    public void die() {
        gameObject.transform.position.x = 0;
        gameObject.transform.position.y = 30;

        Window.getInstance().getCurrentScene().camera.position.x = 0;
    }

    @Override
    public void draw(Graphics2D g2) {
        // AffineTransformer will create simulate coordinates and after make them real
        AffineTransform transform = new AffineTransform();
        // Reset
        transform.setToIdentity();
        // We have to follow a order, (translate, rotate and finally scale)
        transform.translate(gameObject.transform.position.x, gameObject.transform.position.y);
        transform.rotate(gameObject.transform.rotate,
                        // To center the rotation:
                (width * gameObject.transform.scale.x) / 2.0, (height * gameObject.transform.scale.y) / 2.0);
        transform.scale(gameObject.transform.scale.x, gameObject.transform.scale.y);

        // Draw
        g2.drawImage(layerOne.image, transform, null);
        g2.drawImage(layerTwo.image, transform, null);
        g2.drawImage(layerThree.image, transform, null);
    }

    @Override
    public String serialize(int tabSize) {
        return "";
    }
}

package com.davidalmarinho.game_objects.components;

import com.davidalmarinho.utils.Constants;
import com.davidalmarinho.utils.Vector2;

public class RigidBody extends Component {
    public Vector2 velocity;

    public RigidBody(Vector2 velocity) {
        this.velocity = velocity;
    }

    @Override
    public Component copy() {
        return null;
    }

    @Override
    public void update(double dt) {
        gameObject.transform.position.y += velocity.y * dt;
        gameObject.transform.position.x += velocity.x * dt;

        velocity.y += Constants.GRAVITY * dt;

        // Lock speed if its too high
        if (Math.abs(velocity.y) > Constants.TERMINAL_VELOCITY) {
            velocity.y = Math.signum(velocity.y) * Constants.TERMINAL_VELOCITY;
        }
    }

    @Override
    public String serialize(int tabSize) {
        return "";
    }
}

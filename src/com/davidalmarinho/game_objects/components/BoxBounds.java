package com.davidalmarinho.game_objects.components;

import com.davidalmarinho.file.Parser;
import com.davidalmarinho.game_objects.GameObject;
import com.davidalmarinho.utils.Vector2;

public class BoxBounds extends Bounds {
    public float width, height;
    private float halfWidth;
    private float halfHeight;
    private Vector2 center = new Vector2();

    public BoxBounds(float width, float height) {
        this.width = width;
        this.height = height;
        this.halfWidth = this.width / 2.0f;
        this.halfHeight = this.height / 2.0f;
        this.type = BoundsTypes.BOX;
    }

    @Override
    public void start() {
        this.calculateCenter();
    }

    public void calculateCenter() {
        this.center.x = this.gameObject.transform.position.x + this.halfWidth;
        this.center.y = this.gameObject.transform.position.y + this.halfHeight;
    }

    @Override
    public void update(double dt) {
        // System.out.println("We are inside box bounds!");
    }

    public static boolean checkCollision(BoxBounds b1, BoxBounds b2) {
        b1.calculateCenter();
        b2.calculateCenter();

        float dx = b2.center.x - b1.center.x;
        float dy = b2.center.y - b1.center.y;

        float combinedHalfWidths = b1.halfWidth + b2.halfWidth;
        float combinedHalfHeights = b1.halfHeight + b2.halfHeight;

        // Check if they are colliding
        if (Math.abs(dx) <= combinedHalfWidths) { // Checks x axis
            return Math.abs(dy) <= combinedHalfHeights; // Will return if the y axis is also colliding
        }

        return false;
    }

    protected void resolveCollision(GameObject player) {
        BoxBounds playerBounds = player.getComponent(BoxBounds.class);

        playerBounds.calculateCenter();
        this.calculateCenter();

        float dx = this.center.x - playerBounds.center.x;
        float dy = this.center.y - playerBounds.center.y;

        float combinedHalfWidths = playerBounds.halfWidth + this.halfWidth;
        float combinedHalfHeights = playerBounds.halfHeight + this.halfHeight;

        float overLapX = combinedHalfWidths - Math.abs(dx);
        float overlapY = combinedHalfHeights - Math.abs(dy);

        if (overLapX >= overlapY) {
            // Collision on the top of the Player
            if (dy > 0) {
                player.transform.position.y = gameObject.transform.position.y - playerBounds.getHeight();
                player.getComponent(RigidBody.class).velocity.y = 0;
                player.getComponent(Player.class).onGround = true;
            // Collision on the bottom of the Player
            } else {
                player.getComponent(Player.class).die();
            }
        } else {
            // If cube very near from top, we will "climb" the block
            if (dx < 0 && dy <= 0.3) {
                player.transform.position.y = gameObject.transform.position.y - playerBounds.getHeight();
                player.getComponent(RigidBody.class).velocity.y = 0;
                player.getComponent(Player.class).onGround = true;
            } else {
                player.getComponent(Player.class).die();
            }
        }
    }

    @Override
    public Component copy() {
        return new BoxBounds(width, height);
    }

    @Override
    public String serialize(int tabSize) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(beginObjectProperty("BoxBounds", tabSize));
        stringBuilder.append(addFloatProperty("Width", this.width, tabSize + 1, true, true));
        stringBuilder.append(addFloatProperty("Height", this.height, tabSize + 1, true, false));
        stringBuilder.append(closeObjectProperty(tabSize));

        return stringBuilder.toString();
    }

    public static BoxBounds deserialize() {
        float width = Parser.consumeFloatProperty("Width");
        Parser.consume(',');
        float height = Parser.consumeFloatProperty("Height");
        Parser.consumeEndObjectProperty();

        return new BoxBounds(width, height);
    }

    @Override
    public float getWidth() {
        return this.width;
    }

    @Override
    public float getHeight() {
        return this.height;
    }
}

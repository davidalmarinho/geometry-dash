package com.davidalmarinho.game_objects.components;

import com.davidalmarinho.game_objects.GameObject;
import com.davidalmarinho.graphics.Window;
import com.davidalmarinho.main.scenes.LevelScene;
import com.davidalmarinho.utils.Constants;

import java.awt.Graphics2D;
import java.awt.Color;

public class Ground extends Component {

    @Override
    public Component copy() {
        return null;
    }

    @Override
    public void update(double dt) {
        if (!Window.getInstance().inLevelEditorScene) {
            LevelScene scene = (LevelScene) Window.getInstance().getCurrentScene();
            GameObject player = scene.player;

            if (player.transform.position.y + player.getComponent(BoxBounds.class).height >
                    gameObject.transform.position.y) {
                player.transform.position.y = gameObject.transform.position.y - player.getComponent(BoxBounds.class).height;

                player.getComponent(Player.class).onGround = true;
            }
            gameObject.transform.position.x = scene.camera.position.x - 10;
        } else {
            gameObject.transform.position.x = Window.getInstance().getCurrentScene().camera.position.x - 10;
        }
    }

    @Override
    public void draw(Graphics2D g2) {
        g2.setColor(Color.BLACK);
        g2.drawRect((int) gameObject.transform.position.x - 80, (int) gameObject.transform.position.y,
                Constants.SCREEN_WIDTH + 160, Constants.SCREEN_HEIGHT);
    }

    @Override
    public String serialize(int tabSize) {
        return "";
    }
}

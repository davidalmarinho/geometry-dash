package com.davidalmarinho.game_objects.components;

import com.davidalmarinho.game_objects.GameObject;
import com.davidalmarinho.graphics.Window;
import com.davidalmarinho.main.data_structures.AssetPool;
import com.davidalmarinho.utils.Constants;

import java.awt.Graphics2D;

public class ParallaxBackground extends Component {
    private int width, height;
    public Sprite sprite;
    public GameObject[] backgrounds;
    public int timeStep = 0;
    private float speed = 80.0f;

    private Ground ground;
    private boolean followGround;

    public ParallaxBackground(String file, GameObject[] backgrounds, Ground ground, boolean followGround) {
        this.sprite = AssetPool.getSprite(file);
        this.width = this.sprite.width;
        this.height = this.sprite.height;
        this.backgrounds = backgrounds;
        this.ground = ground;
        if (followGround) this.speed = Constants.PLAYER_SPEED - 35;
        this.followGround = followGround;
    }

    @Override
    public void update(double dt) {
        this.timeStep++;

        // Moves background to left
        this.gameObject.transform.position.x -= dt * speed;

        // Remove gap from float
        this.gameObject.transform.position.x = (float) Math.floor(this.gameObject.transform.position.x);

        // If the background is outside of our screen on left
        if (this.gameObject.transform.position.x < -width) {
            // Initialize variables
            float maxX = 0;
            int otherTimeStep = 0;

            // Go throw each game object
            for (GameObject go : backgrounds) {
                if (go.transform.position.x > maxX) {
                    // Pick x position and timeStep from the first background
                    maxX = go.transform.position.x;
                    otherTimeStep = go.getComponent(ParallaxBackground.class).timeStep;
                }
            }

            float xPositionToPutImageInRenderQueue = maxX + width;
            if (otherTimeStep == this.timeStep) {
                this.gameObject.transform.position.x = xPositionToPutImageInRenderQueue;
            } else {
                this.gameObject.transform.position.x = (float) Math.floor((xPositionToPutImageInRenderQueue) -
                        (dt * speed));
            }
        }

        // Align ground's y
        if (this.followGround) {
            this.gameObject.transform.position.y = ground.gameObject.transform.position.y;
        }
    }

    @Override
    public void draw(Graphics2D g2) {
        if (followGround) {
            g2.drawImage(this.sprite.image,
                    (int) this.gameObject.transform.position.x,
                    (int) (gameObject.transform.position.y - Window.getInstance().getCurrentScene().camera.position.y),
                    null);
        } else {
            int height = Math.min((int)(ground.gameObject.transform.position.y -
                    Window.getInstance().getCurrentScene().camera.position.y), Constants.SCREEN_HEIGHT);
            g2.drawImage(this.sprite.image,
                    (int) this.gameObject.transform.position.x, (int) gameObject.transform.position.y,
                    width, Constants.SCREEN_HEIGHT, null);
            g2.setColor(Constants.GROUND_COLOR);
            g2.fillRect((int)this.gameObject.transform.position.x,
                    height, width, Constants.SCREEN_HEIGHT);
        }
    }

    @Override
    public String serialize(int tabSize) {
        return null;
    }

    @Override
    public Component copy() {
        return null;
    }
}

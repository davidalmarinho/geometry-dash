package com.davidalmarinho.inputs;

import com.davidalmarinho.utils.Constants;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MouseListener extends MouseAdapter {
    public boolean mousePressed;
    public boolean mouseDragged;
    public int mouseButton;
    public float x = -1.0f, y = -1.0f;
    public float dx = -1.0f, dy = -1.0f;

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
         mousePressed = true;
         mouseButton = mouseEvent.getButton();
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        mousePressed = false;
        mouseDragged = false;
        dx = 0;
        dy = 0;
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
        x = mouseEvent.getX() / Constants.BACKGROUND_SCALE.x;
        y = mouseEvent.getY() / Constants.BACKGROUND_SCALE.y;
    }

    @Override
    // TODO end this
    public void mouseDragged(MouseEvent mouseEvent) {
        mouseDragged = true;
        // Since we start dragging, we will save the positions
        dx = (mouseEvent.getX() - x);// / Constants.BACKGROUND_SCALE.x;
        dy = (mouseEvent.getY() - y);// / Constants.BACKGROUND_SCALE.y;
    }
}
